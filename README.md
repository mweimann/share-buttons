<!--
SPDX-FileCopyrightText: 2019 Free Software Foundation Europe e.V. <https://fsfe.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Share buttons (bar + left-side-scrolling)

![Screenshot](screenshots/screenshot.png)

These share buttons can be used for various websites, enabling its 
visitors to share specific URLs on social networks, or to enable them 
to do micro-domnations via flattr.

The buttons are much more user-friendly than many other solutions:

- There is absolutely no JavaScript required
- No third-party resources are loaded
- Users of Mastodon, GNU Social and Diaspora can easily provide their pod URL and 
  use the native sharing API

## Location

The buttons are currently available as a usual "share bar", so all 
buttons in one line (see [Screenshot 1](screenshots/screenshot.png)), and as a 
left-side vertical bar scrolling with the screen (see [Screenshot 
2](screenshots/screenshot-side.png)).

## Caveats

The detection of which network the entered Fediverse pod belongs to is
hacky. We check error codes of certain unique URLs. But since the
different Fediverse APIs and structures can be similar, might results
might appear. Contributions for a better detection are appreciated!

## Extendability

Buttons can easily be added and removed by a few lines of code in the 
HTML, CSS, and PHP files.

- In the HTML file, you can just copy/paste the exiting code and modify 
  it. Note that services with user-dependent URLs (like Diaspora and GNU 
  Social) are a bit more complicated and require another GET parameter to 
  be added (like `fediversepod`) which has to be read by `share.php`.
- In the PHP file the parameters are put together, depending on the 
  format the service understands.
- The CSS files keeps all layout information.

## Contributors

The buttons have been designed by Paul Hänsch and Max Mehl in their
capacity as [FSFE](https://fsfe.org) webmasters. Current maintenainer
is @max.mehl, but there are other contributors as well. Please check
the file headers.

## License

GNU Affero General Public License 3.0 or any later version (AGPL-3.0-or-later).
